﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DestroObject : MonoBehaviour 
{
	public Slider life;
	public GameObject hero;
	public GameObject enemy;

	void OnTriggerEnter2D(Collider2D other)
	{
		enemy.GetComponentInChildren<Animator> ().SetBool ("Attack", true);
		hero.GetComponentInChildren<Animator> ().SetBool ("Damage", true);
		life.value = life.value - 10.0f;
		if (life.value <= 0) 
		{
			SceneManager.LoadScene ("GameOver");
		}
		Destroy (other.gameObject);
	}

}
