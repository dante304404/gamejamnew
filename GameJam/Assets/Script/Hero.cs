﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour 
{
	public float heroSpeed;
	public KeyCode left;
	public KeyCode right;
	public KeyCode action;
	public bool move;

	void Start()
	{
		move = true;
	}

	void Update () 
	{
		if (move) 
		{
			if (Input.GetKey(left)) 
			{
				if (!GetComponentInChildren<Animator> ().GetBool("Move")) 
				{
					Debug.Log ("Test");
					GetComponentInChildren<Animator> ().SetBool ("Move", true);
				}
				if (GetComponentInChildren<SpriteRenderer> ().flipX == false)
				{
					GetComponentInChildren<SpriteRenderer> ().flipX = true;
				}

				transform.position += Vector3.right * -heroSpeed * Time.deltaTime;
			}
			if (Input.GetKey(right)) 
			{
				if (!GetComponentInChildren<Animator> ().GetBool("Move")) 
				{
					GetComponentInChildren<Animator> ().SetBool ("Move", true);
				}

				if (GetComponentInChildren<SpriteRenderer> ().flipX == true)
				{
					GetComponentInChildren<SpriteRenderer> ().flipX = false;
				}

				transform.position += Vector3.right * heroSpeed * Time.deltaTime;
			}	
			if (Input.GetKeyUp(left)) 
			{
				GetComponentInChildren<Animator> ().SetBool ("Move", false);
			}
			if (Input.GetKeyUp(right)) 
			{
				GetComponentInChildren<Animator> ().SetBool ("Move", false);
			}	
		}

	}
}
