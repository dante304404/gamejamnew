﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

[XmlRoot("SettingsCollection")]
public class GameSettings
{
    [XmlArray("Settings")]
    [XmlArrayItem("Setting")]
    public List<Settings> settings = new List<Settings>();


    public static GameSettings Load(string path)
    {
        TextAsset _xml = Resources.Load<TextAsset>(path);

        XmlSerializer serializer = new XmlSerializer(typeof(GameSettings));

        StringReader reader = new StringReader(_xml.text);

        GameSettings settings = serializer.Deserialize(reader) as GameSettings;

        reader.Close();

        return settings;
    }

    public static void SetLanguage(string language)
    {
        string settingsText = "";
        bool debug = false;
        try
        {
            settingsText = System.IO.File.ReadAllText("Resources\\settings.xml");
        }
        catch
        {
            debug = true;
            settingsText = System.IO.File.ReadAllText("Assets\\Resources\\settings.xml");
        }


        Regex regex = new Regex("language\\s=\\s\"PL\"");
        Match match = regex.Match(settingsText);
        if (match.Success)
        {
            settingsText = settingsText.Replace("PL", "ENG");
            Debug.Log("Zmienil jezyk na ENG");
        }
        if (!match.Success)
        {
            settingsText = settingsText.Replace("ENG", "PL");
            Debug.Log("Zmienil jezyk na PL");
        }

        if (debug)
        {
            File.WriteAllText("Assets\\Resources\\settings.xml", settingsText);
        }
        else
        {
            File.WriteAllText("Resources\\settings.xml", settingsText);
        }
        

    }




}
