﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnterMouseSign : MonoBehaviour {

	public GameObject sign;
	public Sprite openSign;
	public Sprite closeSign;

	public void OnMouseEnter()
	{
		Debug.Log ("Open");
		sign.GetComponent<Image> ().sprite = openSign;
	}

	public void OnMouseExit()
	{
		Debug.Log ("Exit");
		sign.GetComponent<Image> ().sprite = closeSign;
	}
}
