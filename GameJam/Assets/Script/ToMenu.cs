﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMenu : MonoBehaviour
{
	public KeyCode menuKey;

	void Update ()
	{
		if (Input.GetKey (menuKey)) 
		{
			SceneManager.LoadScene ("Menu");
		}
	}
}
