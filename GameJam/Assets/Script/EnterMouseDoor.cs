﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnterMouseDoor : MonoBehaviour 
{

	public GameObject door;
	public Sprite opendoor;
	public Sprite closedoor;

	public void Open()
	{
		Debug.Log ("Open");
		door.GetComponent<Image> ().sprite = opendoor;
	}


	public void Close()
	{
		Debug.Log ("Exit");
		door.GetComponent<Image> ().sprite = closedoor;
	}

}
