﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour 
{
	public KeyCode skipKey;

	void Update ()
	{
		if (Input.GetKey (skipKey)) 
		{
			SceneManager.LoadScene ("Room1");;
		}
	}
}
