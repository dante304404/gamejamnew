﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour 
{
	public float life;
	public Slider lifeSlider;
	public string scenToLoad;

	// Use this for initialization
	void Start () 
	{
		life = lifeSlider.value;
	}

	public void GetDamage()
	{
		life = life - 10.0f;
		lifeSlider.value = life;
		if (life <= 0) 
		{
			Death ();
		}
	}

	void Death()
	{
		SceneManager.LoadScene(scenToLoad);

	}
}

