﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsTextMove : MonoBehaviour
{
    public GameObject start;
    public GameObject stop;
    public GameObject credits;
    public float speed;

	void Start ()
    {
        credits.transform.position = start.transform.position;
	}
	

	void Update ()
    {
        if (credits.transform.position.y >= stop.transform.position.y)
        {

        }
        else
        {
            credits.transform.position += Vector3.up * speed * Time.deltaTime;
        }

    }
}
