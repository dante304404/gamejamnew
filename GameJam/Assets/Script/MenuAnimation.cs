﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuAnimation : MonoBehaviour
{
    #region sign variable
    public GameObject sign;
    public Sprite openSign;
    public Sprite closeSign;
    #endregion

    #region postmail variable
    public GameObject postmail;
    public Sprite openBoxmail;
    public Sprite closeBoxmail;
    #endregion

    #region door variable
    public GameObject door;
    public Sprite opendoor;
    public Sprite closedoor;
    #endregion

    #region language variable
    public string plNewGame;
    public string engNewGame;

    public string plCredits;
    public string engCredits;

    public string plExit;
    public string engExit;

    public string plSettings;
    public string engSettings;

    public string plHowToPlay;
    public string engHowToPlay;
    #endregion

    public Text text;
    private SettingsLoad gameSettings;

    private void Start()
    {
       var gameObjectSettings = GameObject.FindGameObjectWithTag("Settings");
       gameSettings = gameObjectSettings.GetComponent<SettingsLoad>();
    }


    public void OnSignEnter()
    {
        Debug.Log("Open");
        sign.GetComponent<Image>().sprite = openSign;
        if (gameSettings.language == "PL" || gameSettings.language == "pl" || gameSettings.language == "Pl" || gameSettings.language == "pL")
        {
            text.text = plExit;
        }
        else
        {
            text.text = engExit;
        }
        
    }

    public void OnSignExit()
    {
        text.text = "";
        Debug.Log("Exit");
        sign.GetComponent<Image>().sprite = closeSign;
   
    }

    public void OnBoxMailEnter()
    {
        Debug.Log("Open");
        postmail.GetComponent<Image>().sprite = openBoxmail;
        if (gameSettings.language == "PL" || gameSettings.language == "pl" || gameSettings.language == "Pl" || gameSettings.language == "pL")
        {
            text.text = plCredits;
        }
        else
        {
            text.text = engCredits;
        }
    }


    public void OnBoxMailExit()
    {
        text.text = "";
        Debug.Log("Exit");
        postmail.GetComponent<Image>().sprite = closeBoxmail;
       
    }

    public void OnDoorEnter()
    {
        Debug.Log("Open");
        door.GetComponent<Image>().sprite = opendoor;
        if (gameSettings.language == "PL" || gameSettings.language == "pl" || gameSettings.language == "Pl" || gameSettings.language == "pL")
        {
            text.text = plNewGame;
        }
        else
        {
            text.text = engNewGame;
        }
    }

    public void OnDoorExit()
    {
        text.text = "";
        Debug.Log("Exit");
        door.GetComponent<Image>().sprite = closedoor;
      
    }

    public void OnSettingsEnter()
    {
        if (gameSettings.language == "PL" || gameSettings.language == "pl" || gameSettings.language == "Pl" || gameSettings.language == "pL")
        {
            text.text = plSettings;
        }
        else
        {
            text.text = engSettings;
        }
    }

    public void OnSettingsExit()
    {
        text.text = "";
    }

    public void OnHowToPlayEnter()
    {
        if (gameSettings.language == "PL" || gameSettings.language == "pl" || gameSettings.language == "Pl" || gameSettings.language == "pL")
        {
            text.text = plHowToPlay;
        }
        else
        {
            text.text = engHowToPlay;
        }
    }

    public void OnHowToPlayExit()
    {
        text.text = "";
    }

}
