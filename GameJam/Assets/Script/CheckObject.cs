﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckObject : MonoBehaviour 
{
	public KeyCode KeyToPress;
	public GameObject hero;
	public GameObject enemy;

	void Update()
	{
		if (Input.GetKeyDown (KeyToPress))
		{
			Debug.Log ("KeyPress");
			//GetComponentInChildren<Animator> ().SetBool ("Press",true);
		}
		if (Input.GetKeyUp (KeyToPress))
		{
			Debug.Log ("KeyRelase");
			//GetComponentInChildren<Animator> ().SetBool ("Press",false);
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		Press (other);
	}


	void Press(Collider2D other)
	{
		if (Input.GetKey(KeyToPress)) 
		{
			Debug.Log ("Destroy");
			hero.GetComponentInChildren<Animator> ().SetBool ("Attack", true);
			enemy.GetComponentInChildren<Animator> ().SetBool ("Damage", true);
			Destroy (other.gameObject);
			enemy.GetComponent<Enemy> ().GetDamage ();
		}
	}
}
