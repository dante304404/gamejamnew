﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnterMousePostmail : MonoBehaviour {

	public GameObject postmail;
	public Sprite openBoxmail;
	public Sprite closeBoxmail;

	public void OnMouseEnter()
	{
		Debug.Log ("Open");
		postmail.GetComponent<Image> ().sprite = openBoxmail;
	}


	public void OnMouseExit()
	{
		Debug.Log ("Exit");
		postmail.GetComponent<Image> ().sprite = closeBoxmail;
	}
}


