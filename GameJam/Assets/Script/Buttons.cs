﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{
    public GameObject settingsPanel;
    public GameObject howToPlayPanel;
    public Text howToPlayPanelText;
    public Text howToPlayPanelTextHeader;
    public string howToPlayPanelUpTextPL;
    public string howToPlayPanelDownTextPL;
    public string howToPlayPanelUpTextENG;
    public string howToPlayPanelDownTextENG;
    public string plTextLanguage;
    public string engTextLanguage;
    public Text languageText;
    public Text buttonLanguageText;
    public SettingsLoad settingsLoad;


    public void ButtonStart()
	{
		SceneManager.LoadScene("SplashScreen");
	}

	public void ButtonCredits()
	{
		SceneManager.LoadScene("Credits");
	}

    public void ButtonSettings()
    {
        if (settingsPanel.activeSelf)
        {
            settingsPanel.SetActive(false);
        }
        else
        {
            settingsPanel.SetActive(true);
            ButtonLanguageChangeText();
        }
    }

    public void ButtonHowToPlay()
    {
        if (howToPlayPanel.activeSelf)
        {
            howToPlayPanel.SetActive(false);
        }
        else
        {
            howToPlayPanel.SetActive(true);
            if (settingsLoad.language == "PL")
            {
                howToPlayPanelTextHeader.text = howToPlayPanelUpTextPL;
                howToPlayPanelText.text = howToPlayPanelDownTextPL;
            }
            else
            {
                howToPlayPanelTextHeader.text = howToPlayPanelUpTextENG;
                howToPlayPanelText.text = howToPlayPanelDownTextENG;
            }

        }
    }

    public void ButtonLanguage()
    {
        if (settingsLoad.language == "PL")
        {
            settingsLoad.language = "ENG";
            GameSettings.SetLanguage("ENG");

        }
        else
        {
            settingsLoad.language = "PL";
            GameSettings.SetLanguage("PL");

        }
    }

    public void ButtonLanguageChangeText()
    {
        if (settingsLoad.language == "PL")
        {
            languageText.text = plTextLanguage;
            buttonLanguageText.text = "POLSKI";
            howToPlayPanelTextHeader.text = howToPlayPanelUpTextPL;
            howToPlayPanelText.text = howToPlayPanelDownTextPL;
        }
        else
        {
            languageText.text = engTextLanguage;
            buttonLanguageText.text = "ENGLISH";
            howToPlayPanelTextHeader.text = howToPlayPanelUpTextENG;
            howToPlayPanelText.text = howToPlayPanelDownTextENG;
        }
    }

    public void ButtonLanguageChangeText(string lang)
    {
        if (settingsLoad.language == lang)
        {
            languageText.text = engTextLanguage;
            buttonLanguageText.text = "ENGLISH";
        }
        else
        {
            languageText.text = plTextLanguage;
            buttonLanguageText.text = "POLSKI";
        }
    }

    public void ButtonQuit()
	{
		Application.Quit();
	}
}
