﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAnimation : MonoBehaviour 
{
	public void  StopAttack()
	{
		GetComponent<Animator>().SetBool("Attack", false);
	}

	public void  StopDamage()
	{
		GetComponent<Animator>().SetBool("Damage", false);
	}


}
