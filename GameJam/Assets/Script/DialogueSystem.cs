﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour 
{
	public GameObject hero;
	public GameObject mirorHero;
	public GameObject dialogue;
	public List<string> toSay;
	public List<AudioClip> audioToSay;
	public Sprite talkingPerson;
	public GameObject voice;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			Debug.Log ("Postać ma kontakt z triggerem dialogu");
			hero.GetComponent<Hero> ().move = false;
			hero.GetComponentInChildren<Animator> ().SetBool ("Move", false);
			dialogue.SetActive (true);
			dialogue.GetComponentInChildren<Image> ().sprite = talkingPerson;
			dialogue.GetComponentInChildren<Text> ().text = toSay[0];
			dialogue.GetComponent<ShowDialog> ().line = 0;
			dialogue.GetComponent<ShowDialog> ().toSay = toSay;
			dialogue.GetComponent<ShowDialog> ().audioToSay = audioToSay;
			voice.GetComponent<AudioSource> ().clip = audioToSay [0];
			voice.GetComponent<AudioSource> ().Play ();
			dialogue.GetComponent<ShowDialog> ().dialogActionToDestroy = this.gameObject;
		}
		if (mirorHero != null) 
		{
			mirorHero.GetComponent<Hero> ().move = false;
			mirorHero.GetComponentInChildren<Animator> ().SetBool ("Move", false);
		}
	}

}
