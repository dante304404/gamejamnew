﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ObjectAction : MonoBehaviour 
{
	public bool isActive;
	public bool isinteractObject;
	public bool closed;
	public GameObject dialogBox;
	public string setText;
	public KeyCode action;
	public string scene;

	void Update()
	{
		if (isActive && Input.GetKeyDown(action) && !isinteractObject && !closed) 
		{
			Debug.Log ("Postać przeszła przez drzwi. Rozpoczęcia ładowania następnego poziomu.");
			SceneManager.LoadScene (scene);
		}
		if (isActive && Input.GetKeyDown(action) && isinteractObject) 
		{
			GetComponent<Animator> ().SetBool ("Action", true);
			Debug.Log ("Rozpoczęcie animacji");
		}
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			Debug.Log ("Postać ma kontakt z drzwiami");
			isActive = true;
			dialogBox.SetActive(true);
			SetText (dialogBox.GetComponentInChildren<Text> ());
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			Debug.Log ("Postać traci kontakt z drzwiami");
			isActive = false;
			dialogBox.SetActive(false);
		}
	}

	void SetText(Text textToSet)
	{
		textToSet.text = setText;
	}

}
