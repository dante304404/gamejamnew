﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomElements : MonoBehaviour 
{
	public List<GameObject> lineList;
	public List<GameObject> objectList;
	public float timeSpawn;
	float timer = 0f;

	void Update()
	{

		timer += Time.deltaTime;

		if (timer > timeSpawn) 
		{
			var randomObject = Random.Range (0, objectList.Count);
			var randomList = Random.Range (0, lineList.Count);
			var newPosition = lineList [randomList].transform.position;
			newPosition = new Vector3 (lineList [randomList].transform.position.x,lineList [randomList].transform.position.y,0);
			Instantiate (objectList [randomObject],newPosition,lineList[randomList].transform.rotation);
			timer = 0.0f;
		}


	}

}
