﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowDialog : MonoBehaviour 
{

	public List<string> toSay;
	public int line;
	public KeyCode action;
	public GameObject hero;
	public GameObject mirorHero;
	public GameObject dialogActionToDestroy;
	public List<AudioClip> audioToSay;
	public GameObject voice;

	void Update ()
	{
		if (Input.GetKeyDown (action) && line >= toSay.Count - 1) 
		{
			hero.GetComponent<Hero> ().move = true;
			if (mirorHero != null) 
			{
				mirorHero.GetComponent<Hero> ().move = true;
			}
			dialogActionToDestroy.SetActive (false);
			this.gameObject.SetActive (false);
		}
		if (Input.GetKeyDown (action) && line < toSay.Count) 
		{
			line++;
			if (line < toSay.Count) 
			{
				voice.GetComponent<AudioSource> ().clip = audioToSay [line];
				voice.GetComponent<AudioSource> ().Play ();
				GetComponentInChildren<Text>().text = toSay[line];
			}
		
		}

	}
}
