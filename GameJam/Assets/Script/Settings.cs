﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class Settings
{
    [XmlAttribute("language")]
    public string language;
}
