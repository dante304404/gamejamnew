﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnLoad : MonoBehaviour 
{
	GameObject musicPlayer;
	public AudioClip toPlay;

	void Awake()
	{
		musicPlayer = GameObject.FindWithTag ("MusicPlayer");
		musicPlayer.GetComponent<AudioSource> ().clip = toPlay;
		musicPlayer.GetComponent<AudioSource> ().Play();
	}

}
