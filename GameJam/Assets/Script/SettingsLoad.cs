﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsLoad : MonoBehaviour
{
    public const string path = "settings";
    public string language;

    private static SettingsLoad _instance;

    void Awake()
    {

        if (!_instance)
            _instance = this;
        else
            Destroy(this.gameObject);


        DontDestroyOnLoad(this.gameObject);
    }

    void Start ()
    {
        setDefault();
        Debug.Log("Ustawienia domyślne ustawione");
    }

    public void setDefault()
    {
        GameSettings gameSettings = GameSettings.Load(path);
        Settings setting = gameSettings.settings[0];
        language = setting.language;
    }

    public void setLanguage()
    {
        GameSettings gameSettings = GameSettings.Load(path);
        Settings setting = gameSettings.settings[0];
        language = setting.language;
    }
}
