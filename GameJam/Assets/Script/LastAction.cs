﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastAction : MonoBehaviour 
{
	public KeyCode KeyToPress;
	public GameObject hero;
	private bool stay;

	void Update()
	{
		if (Input.GetKeyDown (KeyToPress) && stay)
		{
			hero.GetComponent<Hero> ().move = false;
			hero.GetComponentInChildren<Animator> ().SetBool ("Last", true);
		}
	}

	void OnTriggerEnter2D()
	{
		stay = true;
		Debug.Log ("Stay");

	}

	void OnTriggerExit2D()
	{
		stay = false;
		Debug.Log ("Not Stay");
	}
}
