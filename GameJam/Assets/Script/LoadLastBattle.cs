﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLastBattle : MonoBehaviour 
{
	public string lastBattle;

	void OnTriggerEnter2D()
	{
		SceneManager.LoadScene (lastBattle);

	}

}
